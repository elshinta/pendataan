-- Adminer 4.8.1 MySQL 5.5.5-10.11.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `area_type`;
CREATE TABLE `area_type` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_data`;
CREATE TABLE `audit_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `data` blob DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_audit_data_entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_entry`;
CREATE TABLE `audit_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `user_id` int(11) DEFAULT 0,
  `duration` float DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `request_method` varchar(16) DEFAULT NULL,
  `ajax` int(1) NOT NULL DEFAULT 0,
  `route` varchar(255) DEFAULT NULL,
  `memory_max` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_route` (`route`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_error`;
CREATE TABLE `audit_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `message` mediumtext NOT NULL,
  `code` int(11) DEFAULT 0,
  `file` varchar(512) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `trace` blob DEFAULT NULL,
  `hash` varchar(32) DEFAULT NULL,
  `emailed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_audit_error_entry_id` (`entry_id`),
  KEY `idx_file` (`file`(180)),
  KEY `idx_emailed` (`emailed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_javascript`;
CREATE TABLE `audit_javascript` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  `message` mediumtext NOT NULL,
  `origin` varchar(512) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_audit_javascript_entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_mail`;
CREATE TABLE `audit_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `successful` int(11) NOT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `reply` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` blob DEFAULT NULL,
  `html` blob DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_audit_mail_entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `audit_trail`;
CREATE TABLE `audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `model_id` varchar(255) NOT NULL,
  `field` varchar(255) DEFAULT NULL,
  `old_value` mediumtext DEFAULT NULL,
  `new_value` mediumtext DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_audit_trail_entry_id` (`entry_id`),
  KEY `idx_audit_user_id` (`user_id`),
  KEY `idx_audit_trail_field` (`model`(191),`model_id`(191),`field`(191)),
  KEY `idx_audit_trail_action` (`action`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(32) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `optimistic_lock` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(191) NOT NULL,
  `remark` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_address` (`ip_address`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `blacklist_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `blacklist_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `certificate_signer`;
CREATE TABLE `certificate_signer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT 'Ketua Dewan Pers',
  `label` varchar(191) NOT NULL,
  `date_start` date NOT NULL COMMENT 'Tanggal Mulai',
  `date_end` date DEFAULT NULL COMMENT 'Tanggal Selesai',
  `file_signature` text DEFAULT NULL COMMENT 'Tanda Tangan',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `certificate_signer_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `certificate_signer_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(191) NOT NULL,
  `label` varchar(191) NOT NULL,
  `default_value` text DEFAULT NULL,
  `current_value` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `district`;
CREATE TABLE `district` (
  `id` char(4) NOT NULL,
  `province_id` char(2) NOT NULL,
  `name` varchar(191) NOT NULL,
  `area_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `province_id` (`province_id`),
  KEY `area_type_id` (`area_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL COMMENT 'Pertanyaan',
  `answer` text NOT NULL COMMENT 'Jawaban',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `faq_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` mediumtext DEFAULT NULL,
  `message` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'User Account',
  `response_type` int(11) DEFAULT NULL COMMENT 'Response Type',
  `responded_by` int(11) DEFAULT NULL COMMENT 'Responded by',
  `responded_at` int(11) DEFAULT NULL COMMENT 'Responded at',
  `submitted_at` int(11) DEFAULT NULL COMMENT 'Status Kirim Revisi',
  `remark_revision_created` text DEFAULT NULL COMMENT 'Catatan Izin Revisi',
  `remark_revision_responded` text DEFAULT NULL COMMENT 'Catatan Hasil Revisi',
  `name` varchar(255) DEFAULT NULL COMMENT 'Nama Media',
  `media_type` int(11) DEFAULT NULL COMMENT 'Jenis Media',
  `periodicity` int(11) DEFAULT NULL COMMENT 'Periodisasi',
  `legal_entity_type` int(11) DEFAULT NULL COMMENT 'Jenis Badan Hukum',
  `legal_entity_name` varchar(255) DEFAULT NULL COMMENT 'Nama Badan Hukum',
  `person_in_charge_name` varchar(255) DEFAULT NULL COMMENT 'Nama Penanggungjawab',
  `person_in_charge_ukw_number` varchar(255) DEFAULT NULL COMMENT 'Nomor UKW Penanggungjawab',
  `file_person_in_charge_ukw_certificate` text DEFAULT NULL COMMENT 'Kartu UKW Penanggungjawab',
  `editor_in_chief_name` varchar(255) DEFAULT NULL COMMENT 'Nama Pemimpin Redaksi',
  `editor_in_chief_ukw_number` varchar(255) DEFAULT NULL COMMENT 'Nomor UKW Pemimpin Redaksi',
  `file_editor_in_chief_ukw_certificate` text DEFAULT NULL COMMENT 'Kartu UKW Pemimpin Redaksi',
  `address` text DEFAULT NULL COMMENT 'Alamat Redaksi',
  `province_id` char(2) DEFAULT NULL COMMENT 'Provinsi',
  `district_id` char(4) DEFAULT NULL COMMENT 'Kabupaten/Kota',
  `postal_code` varchar(15) DEFAULT NULL COMMENT 'Kode Pos',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Telepon',
  `faximile` varchar(255) DEFAULT NULL COMMENT 'Faximile',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `website` varchar(255) DEFAULT NULL COMMENT 'Website',
  `latitude` varchar(255) DEFAULT NULL COMMENT 'Latitude',
  `longitude` varchar(255) DEFAULT NULL COMMENT 'Longitude',
  `file_logo` text DEFAULT NULL COMMENT 'Logo',
  `file_deed_of_incorporation` text DEFAULT NULL COMMENT 'Akta Pendirian Perusahaan Pers',
  `file_legalization_of_ministry_of_law` text DEFAULT NULL COMMENT 'Pengesahan Kementerian Hukum & HAM',
  `file_company_code_of_conduct` text DEFAULT NULL COMMENT 'Kode Perilaku Perusahaan Pers',
  `file_company_regulation` text DEFAULT NULL COMMENT 'Peraturan Perusahaan',
  `file_office_of_labor_decree` text DEFAULT NULL COMMENT 'SK Disnaker tentang Pengesahan Peraturan Perusahaan',
  `file_ministry_of_information_decree` text DEFAULT NULL COMMENT 'SK Kemenkominfo Izin Penyiaran Publik',
  `file_public_broadcasting_license` text DEFAULT NULL COMMENT 'Izin Penyiaran Publik',
  `file_isr_device_certificate` text DEFAULT NULL COMMENT 'ISR Sertifikat Perangkat',
  `printing_office_name` varchar(255) DEFAULT NULL COMMENT 'Nama Percetakan',
  `printing_office_address` text DEFAULT NULL COMMENT 'Alamat Percetakan',
  `count_of_employee_permanent` int(11) DEFAULT NULL COMMENT 'Karyawan Tetap (Redaksi)',
  `count_of_employee_contracted` int(11) DEFAULT NULL COMMENT 'Karyawan Kontrak (Redaksi)',
  `count_of_employee_freelance` int(11) DEFAULT NULL COMMENT 'Karyawan Freelance (Redaksi)',
  `count_of_employee_other` int(11) DEFAULT NULL COMMENT 'Karyawan Lainnya (Non Redaksi)',
  `file_employee_permanent` text DEFAULT NULL COMMENT 'File Karyawan Tetap (Redaksi)',
  `file_employee_contracted` text DEFAULT NULL COMMENT 'File Karyawan Kontrak (Redaksi)',
  `file_employee_freelance` text DEFAULT NULL COMMENT 'Karyawan Freelance (Redaksi)',
  `file_employee_other` text DEFAULT NULL COMMENT 'Karyawan Lainnya (Non Redaksi)',
  `file_ukw_certificate_utama` text DEFAULT NULL COMMENT 'Sertifikat Wartawan Utama',
  `status_ukw_certificate_utama` int(11) DEFAULT NULL COMMENT 'Status Sertifikat Wartawan',
  `introduction_of_journalistics_code_of_conduct` text DEFAULT NULL,
  `file_introduction_of_journalistics_code_of_conduct` text DEFAULT NULL COMMENT 'Sosialisasi Kode Etik Jurnalistik',
  `status_introduction_of_journalistics_code_of_conduct` int(11) DEFAULT NULL COMMENT 'Status Sosialisai',
  `journalistics_training_program` text DEFAULT NULL,
  `file_journalistics_training_program` text DEFAULT NULL COMMENT 'Program Pelatihan Jurnalistik',
  `status_journalistics_training_program` int(11) DEFAULT NULL COMMENT 'Status Program Pelatihan',
  `ukw_participation` text DEFAULT NULL,
  `file_ukw_participation` text DEFAULT NULL COMMENT 'Mengikuti Uji Kompetensi Wartawan',
  `status_ukw_participation` int(11) DEFAULT NULL COMMENT 'Status Uji Kompetensi',
  `file_salary_default` text DEFAULT NULL COMMENT 'Gaji Standar',
  `status_salary_default` int(11) DEFAULT NULL COMMENT 'Status Gaji Standar',
  `file_salary_addition` text DEFAULT NULL COMMENT 'Gaji ke 13 / THR',
  `status_salary_addition` text DEFAULT NULL COMMENT 'Status Gaji ke 13 / THR',
  `salary_bonus` varchar(255) DEFAULT NULL COMMENT 'Kepemilikan Saham / Bonus',
  `status_salary_bonus` int(11) DEFAULT NULL COMMENT 'Status Bonus',
  `supplementary_food` varchar(255) DEFAULT NULL COMMENT 'Menyediakan Konsumsi Tambahan',
  `status_supplementary_food` int(11) DEFAULT NULL COMMENT 'Status Konsumsi',
  `labor_union` varchar(255) DEFAULT NULL COMMENT 'Serikat Pekerja',
  `status_labor_union` int(11) DEFAULT NULL COMMENT 'Status Serikat Pekerja',
  `file_ombudsman` text DEFAULT NULL COMMENT 'Ombudsman',
  `status_ombudsman` int(11) DEFAULT NULL COMMENT 'Status Ombudsman',
  `file_legal_handler` text DEFAULT NULL COMMENT 'Divisi Legal / Kuasa Hukum',
  `status_legal_handler` int(11) DEFAULT NULL COMMENT 'Status Divisi Legal / Kuasa Hukum',
  `file_journalist_protection` text DEFAULT NULL COMMENT 'SOP Perlindungan Wartawan',
  `status_journalist_protection` int(11) DEFAULT NULL COMMENT 'Status SOP',
  `file_vision_and_mission` text DEFAULT NULL COMMENT 'Visi & Misi',
  `status_vision_and_mission` int(11) DEFAULT NULL COMMENT 'Status Visi & Misi',
  `periodicity_description` text DEFAULT NULL COMMENT 'Periodisasi Tayang',
  `duration` double DEFAULT NULL COMMENT 'Durasi Tayang (Jam)',
  `duration_description` text DEFAULT NULL COMMENT 'Keterangan Durasi Tayang',
  `content_diversification` int(11) DEFAULT NULL COMMENT 'Keberagaman Isi',
  `content_diversification_description` text DEFAULT NULL COMMENT 'Keterangan Keberagaman Isi',
  `segmentation` int(11) DEFAULT NULL COMMENT 'Segmentasi',
  `segmentation_description` text DEFAULT NULL COMMENT 'Keterangan Segmentasi',
  `language` int(11) DEFAULT NULL COMMENT 'Bahasa',
  `language_description` text DEFAULT NULL COMMENT 'Keterangan Bahasa',
  `circulation` int(11) DEFAULT NULL COMMENT 'Oplah',
  `circulation_description` text DEFAULT NULL COMMENT 'Keterangan Oplah',
  `file_statement_of_documents_authenticity` text DEFAULT NULL COMMENT 'Surat Pernyataan Keaslian Dokumen',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `province_id` (`province_id`),
  KEY `district_id` (`district_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `responded_by` (`responded_by`),
  CONSTRAINT `media_ibfk_2` FOREIGN KEY (`province_id`) REFERENCES `province` (`id`),
  CONSTRAINT `media_ibfk_3` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`),
  CONSTRAINT `media_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_ibfk_7` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_ibfk_8` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_ibfk_9` FOREIGN KEY (`responded_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `media_file`;
CREATE TABLE `media_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL COMMENT 'Media',
  `type` int(11) NOT NULL COMMENT 'Tipe',
  `name` varchar(255) DEFAULT NULL COMMENT 'Nama',
  `file` text DEFAULT NULL COMMENT 'File',
  `group` int(11) DEFAULT NULL COMMENT 'Grup (insurance; physical_condition)',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `media_file_ibfk_4` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_file_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_file_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `media_journalist`;
CREATE TABLE `media_journalist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL COMMENT 'Media',
  `name` varchar(255) NOT NULL COMMENT 'Nama Wartawan',
  `level` int(11) DEFAULT NULL COMMENT 'Status Wartawan',
  `ukw_number` varchar(255) DEFAULT NULL COMMENT 'Nomor Sertifikasi UKW',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `media_journalist_ibfk_4` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_journalist_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `media_journalist_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `province`;
CREATE TABLE `province` (
  `id` char(2) NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `regulation`;
CREATE TABLE `regulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT 'Nomor',
  `description` text NOT NULL COMMENT 'Keterangan',
  `date` date NOT NULL COMMENT 'Tanggal',
  `file` text DEFAULT NULL COMMENT 'File',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `regulation_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `regulation_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `is_trusted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `subdistrict`;
CREATE TABLE `subdistrict` (
  `id` char(6) NOT NULL,
  `district_id` char(4) NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'User',
  `title` varchar(255) NOT NULL COMMENT 'Judul',
  `content` text NOT NULL COMMENT 'Isi',
  `closed_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(191) NOT NULL,
  `username` varchar(191) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(191) DEFAULT NULL,
  `verification_token` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `is_media` int(11) DEFAULT 0,
  `is_migrated` int(11) DEFAULT NULL,
  `id_public` int(11) DEFAULT NULL,
  `revision_status` int(11) DEFAULT 0 COMMENT '0 = no revision, 1 = revision allowed, 2 = revision submitted',
  `certificate_number` varchar(191) DEFAULT NULL,
  `email_origin` varchar(191) DEFAULT NULL,
  `email_duplicate` varchar(191) DEFAULT NULL,
  `must_change_password` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `media_id` (`is_media`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user_status`;
CREATE TABLE `user_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Media',
  `type` int(11) NOT NULL COMMENT 'Status',
  `description` text DEFAULT NULL COMMENT 'Keterangan',
  `file_report` text DEFAULT NULL COMMENT 'Laporan Verifikator',
  `file_photo` text DEFAULT NULL COMMENT 'Dokumentasi',
  `file_related` text DEFAULT NULL COMMENT 'Pendukung',
  `certificate_date` date DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `user_status_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_status_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_status_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `village`;
CREATE TABLE `village` (
  `id` char(10) NOT NULL,
  `subdistrict_id` char(6) DEFAULT NULL,
  `name` tinytext DEFAULT NULL,
  `area_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subdistrict_id` (`subdistrict_id`),
  KEY `area_type_id` (`area_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `_old_media`;
CREATE TABLE `_old_media` (
  `mstmedia_id` int(11) NOT NULL DEFAULT 0,
  `mstmedia_nama` varchar(255) DEFAULT NULL,
  `mstmedia_jenis` varchar(255) DEFAULT NULL,
  `mstmedia_periode` varchar(255) DEFAULT NULL,
  `mstmedia_logo` text DEFAULT NULL,
  `mstmedia_email` varchar(255) DEFAULT NULL,
  `mstmedia_badanhukum` varchar(255) DEFAULT NULL,
  `mstprovinsi_id` int(11) DEFAULT NULL,
  `mstkabupaten_id` int(11) DEFAULT NULL,
  `activerow` int(11) DEFAULT NULL,
  `mstmedia_statusreg` int(1) DEFAULT NULL COMMENT '1; daftar tapi blm activate, 2 : sudah activate, 3 terkunci',
  `mstmedia_activationlink` varchar(255) DEFAULT NULL,
  `cdate` timestamp NULL DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  `actor` varchar(255) DEFAULT NULL,
  `mstmedia_password` varchar(255) DEFAULT NULL,
  `mstmedia_username` varchar(255) DEFAULT NULL,
  `mstmedia_lastlogin` datetime DEFAULT NULL,
  `mstmedia_namaperusahaan` varchar(255) DEFAULT NULL,
  `mstmedia_statuslengkap` int(1) DEFAULT NULL COMMENT '1= lengkap. 0 = belum',
  `mstmedia_sumberInput` int(1) DEFAULT NULL COMMENT '1 = dari user sendiri, 2 = dari admin.',
  `mstmedia_statusAjuan` int(1) DEFAULT NULL COMMENT '1: terkirim . 2 : proses, 3 : diterima, 4 : ditolak. 5. update',
  `mstmedia_ketAjuan` varchar(255) DEFAULT NULL,
  `mstadm_id` int(1) DEFAULT NULL,
  `mstmedia_tglSend` datetime DEFAULT NULL,
  `mstmedia_tglApprove` date DEFAULT NULL,
  `mstmedia_alamat` varchar(255) DEFAULT NULL,
  `mstmedia_notelp` varchar(255) DEFAULT NULL,
  `mstmedia_fax` varchar(255) DEFAULT NULL,
  `mstmedia_website` varchar(255) DEFAULT NULL,
  `mstmedia_pimred` varchar(255) DEFAULT NULL,
  `mstmedia_penanggungjawab` varchar(255) DEFAULT NULL,
  `mstmedia_statusVerifikasi` int(1) DEFAULT NULL,
  `mstmedia_long` varchar(255) DEFAULT NULL,
  `mstmedia_lat` varchar(255) DEFAULT NULL,
  `mstmedia_regulerStatus` int(1) DEFAULT NULL,
  `mstprovinsi_nama1` varchar(255) DEFAULT NULL,
  `mstkabupaten_nama` varchar(255) DEFAULT NULL,
  `statusVerifikasi` varchar(255) DEFAULT NULL,
  `statusAjuan` varchar(255) DEFAULT NULL,
  `activerow_org` int(11) DEFAULT NULL,
  KEY `mstmedia_id` (`mstmedia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP VIEW IF EXISTS `_view_activity`;
CREATE TABLE `_view_activity` (`time` int(11), `user_id` int(11), `type` int(11), `description` mediumtext, `file_report` mediumtext, `file_photo` mediumtext, `file_related` mediumtext, `user_status_id` int(11), `media_id` int(11));


DROP VIEW IF EXISTS `_view_current_key`;
CREATE TABLE `_view_current_key` (`user_id` int(11), `user_status_id` int(11), `media_id` int(11));


DROP VIEW IF EXISTS `_view_current_value`;
CREATE TABLE `_view_current_value` (`user_id` int(11), `id_public` int(11), `user_email` varchar(191), `user_name` varchar(255), `user_certificate_number` varchar(191), `user_created_at` int(11), `user_status_id` int(11), `user_status_type` int(11), `user_status_description` text, `user_status_certificate_date` varchar(10), `user_status_created_at` int(11), `media_id` int(11), `media_name` varchar(255), `media_media_type` int(11), `media_legal_entity_type` int(11), `media_legal_entity_name` varchar(255), `media_person_in_charge_name` varchar(255), `media_person_in_charge_ukw_number` varchar(255), `media_editor_in_chief_name` varchar(255), `media_editor_in_chief_ukw_number` varchar(255), `media_address` text, `media_province_id` char(2), `media_district_id` char(4), `media_postal_code` varchar(15), `media_phone` varchar(255), `media_faximile` varchar(255), `media_email` varchar(255), `media_website` varchar(255), `media_latitude` varchar(255), `media_longitude` varchar(255), `media_created_at` int(11), `media_updated_at` int(11), `media_submitted_at` int(11), `media_responded_at` int(11));


DROP VIEW IF EXISTS `_view_media_first`;
CREATE TABLE `_view_media_first` (`user_id` int(11), `media_id` int(11));


DROP VIEW IF EXISTS `_view_public`;
CREATE TABLE `_view_public` (`user_id` varchar(11), `id_public` int(11), `media_name` varchar(255), `media_media_type` int(11), `media_media_type_name` varchar(9), `media_person_in_charge_name` varchar(255), `media_editor_in_chief_name` varchar(255), `media_legal_entity_name` varchar(255), `media_province_name` text, `media_address` mediumtext, `media_phone` varchar(255), `media_email` varchar(255), `media_website` varchar(255), `user_status_type` varchar(255), `user_status_certificate_date` varchar(10), `user_status_created_at` varchar(10));


DROP VIEW IF EXISTS `_view_revision_created`;
CREATE TABLE `_view_revision_created` (`user_id` int(11), `media_id` int(11), `media_created_at` int(11), `media_remark_revision_created` text);


DROP VIEW IF EXISTS `_view_revision_responded`;
CREATE TABLE `_view_revision_responded` (`user_id` int(11), `media_id` int(11), `media_responded_at` int(11), `media_response_type` int(11), `media_remark_revision_responded` text);


DROP VIEW IF EXISTS `_view_revision_submitted`;
CREATE TABLE `_view_revision_submitted` (`user_id` int(11), `media_id` int(11), `media_submitted_at` int(11));


DROP TABLE IF EXISTS `_view_activity`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_activity` AS select `user_status`.`created_at` AS `time`,`user_status`.`user_id` AS `user_id`,`user_status`.`type` AS `type`,`user_status`.`description` collate utf8mb4_unicode_ci AS `description`,`user_status`.`file_report` collate utf8mb4_unicode_ci AS `file_report`,`user_status`.`file_photo` collate utf8mb4_unicode_ci AS `file_photo`,`user_status`.`file_related` collate utf8mb4_unicode_ci AS `file_related`,`user_status`.`id` AS `user_status_id`,NULL AS `media_id` from `user_status` union select `_view_revision_created`.`media_created_at` AS `time`,`_view_revision_created`.`user_id` AS `user_id`,7 AS `type`,`_view_revision_created`.`media_remark_revision_created` collate utf8mb4_unicode_ci AS `description`,NULL AS `file_report`,NULL AS `file_photo`,NULL AS `file_related`,NULL AS `user_status_id`,`_view_revision_created`.`media_id` AS `media_id` from `_view_revision_created` union select `_view_revision_submitted`.`media_submitted_at` AS `time`,`_view_revision_submitted`.`user_id` AS `user_id`,8 AS `type`,NULL AS `description`,NULL AS `file_report`,NULL AS `file_photo`,NULL AS `file_related`,NULL AS `user_status_id`,`_view_revision_submitted`.`media_id` AS `media_id` from `_view_revision_submitted` union select `_view_revision_responded`.`media_responded_at` AS `time`,`_view_revision_responded`.`user_id` AS `user_id`,if(`_view_revision_responded`.`media_response_type` = 1,10,9) AS `type`,`_view_revision_responded`.`media_remark_revision_responded` collate utf8mb4_unicode_ci AS `description`,NULL AS `file_report`,NULL AS `file_photo`,NULL AS `file_related`,NULL AS `user_status_id`,`_view_revision_responded`.`media_id` AS `media_id` from `_view_revision_responded`;

DROP TABLE IF EXISTS `_view_current_key`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_current_key` AS select `user`.`id` AS `user_id`,max(`user_status`.`id`) AS `user_status_id`,max(`media`.`id`) AS `media_id` from ((`user` left join `user_status` on(`user`.`id` = `user_status`.`user_id`)) left join `media` on(`user`.`id` = `media`.`user_id`)) where `media`.`id` is not null and `media`.`response_type` = 1 and `user`.`id` = `user_status`.`user_id` and `user`.`id` = `media`.`user_id` and `user_status`.`user_id` = `media`.`user_id` group by `user`.`id`;

DROP TABLE IF EXISTS `_view_current_value`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_current_value` AS select `user`.`id` AS `user_id`,`user`.`id_public` AS `id_public`,`user`.`email` AS `user_email`,`user`.`name` AS `user_name`,`user`.`certificate_number` AS `user_certificate_number`,`user`.`created_at` AS `user_created_at`,`user_status`.`id` AS `user_status_id`,`user_status`.`type` AS `user_status_type`,`user_status`.`description` AS `user_status_description`,if(`user_status`.`certificate_date` is not null,`user_status`.`certificate_date`,date_format(from_unixtime(`user_status`.`created_at`),'%Y-%m-%d')) AS `user_status_certificate_date`,`user_status`.`created_at` AS `user_status_created_at`,`media`.`id` AS `media_id`,`media`.`name` AS `media_name`,`media`.`media_type` AS `media_media_type`,`media`.`legal_entity_type` AS `media_legal_entity_type`,`media`.`legal_entity_name` AS `media_legal_entity_name`,`media`.`person_in_charge_name` AS `media_person_in_charge_name`,`media`.`person_in_charge_ukw_number` AS `media_person_in_charge_ukw_number`,`media`.`editor_in_chief_name` AS `media_editor_in_chief_name`,`media`.`editor_in_chief_ukw_number` AS `media_editor_in_chief_ukw_number`,`media`.`address` AS `media_address`,`media`.`province_id` AS `media_province_id`,`media`.`district_id` AS `media_district_id`,`media`.`postal_code` AS `media_postal_code`,`media`.`phone` AS `media_phone`,`media`.`faximile` AS `media_faximile`,`media`.`email` AS `media_email`,`media`.`website` AS `media_website`,`media`.`latitude` AS `media_latitude`,`media`.`longitude` AS `media_longitude`,`media`.`created_at` AS `media_created_at`,`media`.`updated_at` AS `media_updated_at`,`media`.`submitted_at` AS `media_submitted_at`,`media`.`responded_at` AS `media_responded_at` from (((`_view_current_key` left join `user` on(`_view_current_key`.`user_id` = `user`.`id`)) left join `user_status` on(`_view_current_key`.`user_status_id` = `user_status`.`id`)) left join `media` on(`_view_current_key`.`media_id` = `media`.`id`));

DROP TABLE IF EXISTS `_view_media_first`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_media_first` AS select `user`.`id` AS `user_id`,min(`media`.`id`) AS `media_id` from (`user` join `media`) where `user`.`id` = `media`.`user_id` group by `user`.`id`;

DROP TABLE IF EXISTS `_view_public`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_public` AS select `user`.`id` AS `user_id`,`user`.`id_public` AS `id_public`,`media`.`name` AS `media_name`,`media`.`media_type` AS `media_media_type`,if(`media`.`media_type` = 1,'Cetak',if(`media`.`media_type` = 2,'Radio',if(`media`.`media_type` = 3,'Televisi',if(`media`.`media_type` = 4,'Siber',if(`media`.`media_type` = 5,'Fotografi',NULL))))) AS `media_media_type_name`,`media`.`person_in_charge_name` AS `media_person_in_charge_name`,`media`.`editor_in_chief_name` AS `media_editor_in_chief_name`,`media`.`legal_entity_name` AS `media_legal_entity_name`,`province`.`name` AS `media_province_name`,`media`.`address` AS `media_address`,`media`.`phone` AS `media_phone`,`media`.`email` AS `media_email`,`media`.`website` AS `media_website`,if(`user_status`.`type` = 6,'Terverifikasi Administratif dan Faktual','Terverifikasi Administratif') AS `user_status_type`,if(`user_status`.`certificate_date` is not null,`user_status`.`certificate_date`,date_format(from_unixtime(`user_status`.`created_at`),'%Y-%m-%d')) AS `user_status_certificate_date`,date_format(from_unixtime(`user_status`.`created_at`),'%Y-%m-%d') AS `user_status_created_at` from ((((`_view_current_key` left join `user` on(`_view_current_key`.`user_id` = `user`.`id`)) left join `user_status` on(`_view_current_key`.`user_status_id` = `user_status`.`id`)) left join `media` on(`_view_current_key`.`media_id` = `media`.`id`)) left join `province` on(`media`.`province_id` = `province`.`id`)) where `user_status`.`type` >= 5 union select '' AS `Name_exp_1`,`_old_media`.`mstmedia_id` AS `mstmedia_id`,`_old_media`.`mstmedia_nama` AS `mstmedia_nama`,if(`_old_media`.`mstmedia_jenis` = 'cetak',1,if(`_old_media`.`mstmedia_jenis` = 'radio',2,if(`_old_media`.`mstmedia_jenis` = 'tv',3,if(`_old_media`.`mstmedia_jenis` = 'siber',4,if(`_old_media`.`mstmedia_jenis` = 'siaran',3,NULL))))) AS `mstmedia_jenis`,if(`_old_media`.`mstmedia_jenis` = 'cetak','Cetak',if(`_old_media`.`mstmedia_jenis` = 'radio','Radio',if(`_old_media`.`mstmedia_jenis` = 'tv','Televisi',if(`_old_media`.`mstmedia_jenis` = 'siber','Siber',if(`_old_media`.`mstmedia_jenis` = 'siaran','Televisi',NULL))))) AS `media_media_type_name`,`_old_media`.`mstmedia_penanggungjawab` AS `mstmedia_penanggungjawab`,`_old_media`.`mstmedia_pimred` AS `mstmedia_pimred`,`_old_media`.`mstmedia_badanhukum` AS `mstmedia_badanhukum`,`_old_media`.`mstprovinsi_nama1` AS `mstprovinsi_nama1`,`_old_media`.`mstmedia_alamat` AS `mstmedia_alamat`,`_old_media`.`mstmedia_notelp` AS `mstmedia_notelp`,`_old_media`.`mstmedia_email` AS `mstmedia_email`,`_old_media`.`mstmedia_website` AS `mstmedia_website`,`_old_media`.`statusVerifikasi` AS `statusVerifikasi`,`_old_media`.`mstmedia_tglApprove` AS `mstmedia_certificate_date`,`_old_media`.`mstmedia_tglApprove` AS `mstmedia_tglApprove` from `_old_media` where `_old_media`.`activerow` = 1 and (`_old_media`.`statusVerifikasi` = 'Terverifikasi Administrasi' or `_old_media`.`statusVerifikasi` = 'Terverifikasi Administrasi dan Faktual');

DROP TABLE IF EXISTS `_view_revision_created`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_revision_created` AS select `user`.`id` AS `user_id`,`media`.`id` AS `media_id`,`media`.`created_at` AS `media_created_at`,`media`.`remark_revision_created` AS `media_remark_revision_created` from ((`user` left join `media` on(`user`.`id` = `media`.`user_id`)) left join `_view_media_first` on(`media`.`id` = `_view_media_first`.`media_id`)) where `_view_media_first`.`media_id` is null and `media`.`id` is not null;

DROP TABLE IF EXISTS `_view_revision_responded`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_revision_responded` AS select `user`.`id` AS `user_id`,`media`.`id` AS `media_id`,`media`.`responded_at` AS `media_responded_at`,`media`.`response_type` AS `media_response_type`,`media`.`remark_revision_responded` AS `media_remark_revision_responded` from ((`user` left join `media` on(`user`.`id` = `media`.`user_id`)) left join `_view_media_first` on(`media`.`id` = `_view_media_first`.`media_id`)) where `_view_media_first`.`media_id` is null and `media`.`id` is not null and `media`.`responded_at` is not null;

DROP TABLE IF EXISTS `_view_revision_submitted`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_view_revision_submitted` AS select `user`.`id` AS `user_id`,`media`.`id` AS `media_id`,`media`.`submitted_at` AS `media_submitted_at` from ((`user` left join `media` on(`user`.`id` = `media`.`user_id`)) left join `_view_media_first` on(`media`.`id` = `_view_media_first`.`media_id`)) where `_view_media_first`.`media_id` is null and `media`.`id` is not null and `media`.`submitted_at` is not null;

-- 2023-06-25 19:19:32
